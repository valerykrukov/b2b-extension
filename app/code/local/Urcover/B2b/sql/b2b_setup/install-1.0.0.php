<?php 

$installer = $this;
$installer->startSetup();

$table = $installer->getConnection()
	->newTable($installer->getTable('b2b/customers'))
	->addColumn('customer_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
		'identity' => true,
		'unsigned' => true,
		'nullable' => false,
		'primary' => true,
	), 'Id')
	->addColumn('internal_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
		'nullable' => false,
	), 'Internal Id')
	->addColumn('is_approved', Varien_Db_Ddl_Table::TYPE_BOOLEAN, null, array(
		'nullable' => false,
	), 'Is Approved')
	->addColumn('request_at', Varien_Db_Ddl_Table::TYPE_TIMESTAMP, null, array(
		'nullable' => true,
		'default' => null,
	), 'Requested At');

$installer->getConnection()->createTable($table);
$installer->endSetup();