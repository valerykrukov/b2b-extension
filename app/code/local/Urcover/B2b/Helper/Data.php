<?php 

class Urcover_B2b_Helper_Data extends Mage_Core_Helper_Abstract
{
	public function getPostActionUrl()
	{
		return $this->_getUrl('b2b/index/register');
	}
	
	public function getRegionsListUrl()
	{
		return $this->_getUrl('b2b/index/regions');
	}
	
	public function checkForEmailNotExists($email)
	{
		$email = Mage::getSingleton('core/resource')->getConnection('default_write')->quote(trim($email));
		$connection = Mage::getSingleton('core/resource')->getConnection('core_read');
		$sql = 'SELECT COUNT(`entity_id`) FROM `customer_entity` WHERE `email` = '.$email;
		$row = $connection->fetchOne($sql);
		return (intval($row) == 0)?true:false;
	}
	
	public function createCustomer($data)
	{
		$website = Mage::app()->getWebsite()->getId();
		$store = Mage::app()->getStore();
		$customer = Mage::getModel('customer/customer');
		$discountGroup = Mage::getStoreConfig('b2b/main_options/discount_group');
		$customer->setWebsiteId($websiteId)
			->setStore($store)
			->setFirstname($data['first_name'])
			->setLastname($data['last_name'])
			->setEmail($data['email_address'])
			->setGroupId($discountGroup)
			->setStatus(0)
			->setIsActive(0)
			->setConfirmation(null)
			->setPassword($data['password']);
		if (isset($data['middle_name'])) {
			$customer->setMiddleName($data['middle_name']);
		}
		try {
			$customer->save();
		} catch (Exception $e) {
			Zend_Debug::dump($e->getMessage());
			return false;
		}
		$address = Mage::getModel('customer/address');
		$address->setCustomerId($customer->getId())
			->setFirstname($customer->getFirstname())
			->setMiddleName($customer->getMiddlename())
			->setLastname($customer->getLastname())
			->setCountryId($data['country'])
			->setPostcode($data['postcode'])
			->setCity($data['city'])
			->setStreet($data['street'])
			->setCompany($data['company_name'])
			->setIsDefaultBilling('1')
			->setIsDefaultShipping('1')
			->setSaveInAddressBook('0');
		try {
			$address->save();
		} catch (Exception $e) {
			Zend_Debug::dump($e->getMessage());
			return false;
		}
		$b2b = Mage::getModel('b2b/customers');
		$b2b->setInternalId($customer->getId());
		$b2b->setIsApproved(false);
		$b2b->setRequestAt(time());
		try {
			$b2b->save();
		} catch (Exception $e) {
			Zend_Debug::dump($e->getMessage());
			return false;
		}
		return $this->sendNotification($customer, $address);
	}
	
	protected function sendNotification($customer, $address, $data)
	{
		$to = Mage::getStoreConfig('b2b/notification_options/email_to');
		$toName = Mage::getStoreConfig('b2b/notification_options/email_to_name');
		$from = $customer->getEmail();
		$fromName = implode(' ', array(
			$customer->getFirstname(),
			$customer->getMiddlename(),
			$customer->getLastname()
		));
		$body = Mage::getStoreConfig('b2b/notification_options/email_template');
		$body = str_replace(
			array(
				'#USERNAME#',
				'#COMPANY#',
				'#COUNTRY#',
				'#CITY#',
				'#STREET#',
				'#REGION#',
				'#POSTCODE#',
				'#EMAIL#',
			),
			array(
				$fromName,
				$address->getCompany(),
				$address->getCountryId(),
				$address->getCity(),
				isset($data['region'])?$data['region']:'',
				$address->getPostcode(),
				$customer->getEmail(),
			),
			$body
		);
		$mail = Mage::getModel('core/email');
		$mail->setToName($toName);
		$mail->setToEmail($to);
		$mail->setBody($body);
		$mail->setSubject('B2B Request');
		$mail->setFromEmail($from);
		$mail->setFromName($fromName);
		$mail->setType('html');
		try {
			$mail->send();
		} catch (Exception $e) {
			Zend_Debug::dump($e->getMessage());
			return false;
		}
		return true;
	}
}