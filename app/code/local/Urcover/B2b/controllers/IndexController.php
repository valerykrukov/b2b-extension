<?php 

class Urcover_B2b_IndexController extends Mage_Core_Controller_Front_Action
{
	public function indexAction()
	{
		$this->loadLayout();
		$this->renderLayout();
	}
	
	public function registerAction()
	{
		$required = array(
			'first_name',
			'last_name',
			'email_address',
			'company_name',
			'country',
			'city',
			'postcode',
			'street',
			'password',
			'confirmation',
		);
		$sent = $_POST;
		$data = array();
		foreach ($required as $field) {
			if (!isset($sent[$field]) || trim($sent[$field]) == '') {
				Mage::getSingleton('core/session')->addError(Mage::helper('b2b')->__('Field "'.$sent[$field].'" is required.'));
				$this->_redirect('*/*/');
				return;
			} else {
				$data[$field] = trim($sent[$field]);
			}
		}
		if (Mage::helper('b2b')->checkForEmailNotExists($data['email_address']) === true) {
			if (isset($sent['middle_name'])) {
				$data['middle_name'] = trim($sent['middle_name']);
			}
			if (isset($sent['region'])) {
				$data['region'] = trim($sent['region']);
			}
			if (Mage::helper('b2b')->createCustomer($data) !== false) {
				$this->_redirect('*/success');
				return;
			} else {
				Mage::getSingleton('core/session')->addError(Mage::helper('b2b')->__('Error'));
				$this->_redirect('*/*/');
				return;
			}
		} else {
			Mage::getSingleton('core/session')->addError(Mage::helper('b2b')->__('User already registered. He need to wait approve.'));
			$this->_redirect('*/*/');
			return;
		}
	}
}