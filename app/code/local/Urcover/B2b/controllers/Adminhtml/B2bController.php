<?php 

class Urcover_B2b_Adminhtml_B2bController extends Mage_Adminhtml_Controller_Action
{
	public function approveAction()
	{
		$request = $this->getRequest();
		$customer = Mage::getModel('b2b/customers')->load($request->getParam('c'));
		$customer->setIsApproved(true);
		try {
			$customer->save();
			$customer->sendNewAccountEmail('confirmation', '', Mage::app()->getStore()->getId());
		} catch (Exception $e) {
			Zend_Debug::dump($e->getMessage());
			return false;
		}
		$this->_redirect('adminhtml/customer');
		return;
	}
	
	public function disableAction()
	{
		$request = $this->getRequest();
		$customer = Mage::getModel('b2b/customers')->load($request->getParam('c'));
		$customer->setIsApproved(false);
		try {
			$customer->save();
		} catch (Exception $e) {
			Zend_Debug::dump($e->getMessage());
			return false;
		}
		$this->_redirect('adminhtml/customer');
		return;
	}
}