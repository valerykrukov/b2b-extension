<?php 
require_once Mage::getModuleDir('controllers','Mage_Customer').DS.'AccountController.php';

class Urcover_B2b_AccountController extends Mage_Customer_AccountController
{
	public function loginPostAction()
	{
		if (!$this->_validateFormKey()) {
			$this->_redirect('*/*/');
			return;
		}
	
		if ($this->_getSession()->isLoggedIn()) {
			$this->_redirect('*/*/');
			return;
		}
		$session = $this->_getSession();
	
		if ($this->getRequest()->isPost()) {
			$login = $this->getRequest()->getPost('login');
			if (!empty($login['username']) && !empty($login['password'])) {
				try {
					$session->login($login['username'], $login['password']);
					$cid = intval($session->getCustomer()->getGroupId());
					$discountGroup = intval(Mage::getStoreConfig('b2b/main_options/discount_group'));
					if ($cid == $discountGroup) {
						$connection = Mage::getSingleton('core/resource')->getConnection('core_read');
						$sql = 'SELECT is_approved FROM `b2b_customers` WHERE `internal_id` = '.intval($session->getCustomer()->getId());
						$row = $connection->fetchRow($sql);
						if (!isset($row['is_approved']) || intval($row['is_approved']) == 0) {
							$session->logout()->renewSession();
							$session->addError($this->_getHelper('b2b')->__('User is not approved'));
							$session->setUsername($login['username']);
							$this->_redirect('*/*/');
							return;
						}
					}
					if ($session->getCustomer()->getIsJustConfirmed()) {
						$this->_welcomeCustomer($session->getCustomer(), true);
					}
				} catch (Mage_Core_Exception $e) {
					switch ($e->getCode()) {
						case Mage_Customer_Model_Customer::EXCEPTION_EMAIL_NOT_CONFIRMED:
							$value = $this->_getHelper('customer')->getEmailConfirmationUrl($login['username']);
							$message = $this->_getHelper('customer')->__('This account is not confirmed. <a href="%s">Click here</a> to resend confirmation email.', $value);
							break;
						case Mage_Customer_Model_Customer::EXCEPTION_INVALID_EMAIL_OR_PASSWORD:
							$message = $e->getMessage();
							break;
						default:
							$message = $e->getMessage();
					}
					$session->addError($message);
					$session->setUsername($login['username']);
				} catch (Exception $e) {
					// Mage::logException($e); // PA DSS violation: this exception log can disclose customer password
				}
			} else {
				$session->addError($this->__('Login and password are required.'));
			}
		}
	
		$this->_loginPostRedirect();
	}
}