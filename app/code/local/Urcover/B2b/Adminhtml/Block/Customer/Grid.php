<?php 

class Urcover_B2b_Adminhtml_Block_Customer_Grid extends Mage_Adminhtml_Block_Customer_Grid
{
	protected function _prepareColumns()
	{
		$this->addColumn('b2b_approved', array(
			'header' => 'B2B',
			'align' => 'center',
			'index' => 'b2b_approved',
			'width' => '25',
			'renderer' => 'Urcover_B2b_Block_Adminhtml_Template_Grid_Renderer_B2b'
		));
		return parent::_prepareColumns();
	}
}