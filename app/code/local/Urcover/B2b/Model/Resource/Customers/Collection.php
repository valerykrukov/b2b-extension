<?php 

class Urcover_B2b_Model_Resource_Customers_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
{
	public function _construct()
	{
		$this->_init('b2b/customers');
	}
}