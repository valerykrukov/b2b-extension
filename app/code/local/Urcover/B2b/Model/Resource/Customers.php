<?php 

class Urcover_B2b_Model_Resource_Customers extends Mage_Core_Model_Resource_Db_Abstract
{
	protected function _construct()
	{
		$this->_init('b2b/customers', 'customer_id');
	}

}