<?php 

class Urcover_B2b_Model_Observer
{
	public function getWholesalePrice(Varien_Event_Observer $observer)
	{
		$event = $observer->getEvent();
		if (Mage::getSingleton('customer/session')->isLoggedIn()) {
			$discountGroup = Mage::getStoreConfig('b2b/main_options/discount_group');
			$groupId = Mage::getSingleton('customer/session')->getCustomerGroupId();
			if ($groupId == intval($discountGroup)) {
				$product = $event->getProduct();
				if ($product && null != $product->getSku()) {
					$discountPercent = floatval(Mage::getStoreConfig('b2b/main_options/discount_percent'));
					$basePrice = $product->getFinalPrice();
					$discounted = $basePrice - ($basePrice * ($discountPercent / 100));
					$product->setFinalPrice($discounted);
				}
			}
		}
	}
	
	public function addApprovingColumns(Varien_Event_Observer $observer)
	{
		$block = $observer->getBlock();
		if (!isset($block)) {
			return $this;
		}
		if ($block->getType() == 'adminhtml/customer_grid') {
			$block->addColumnAfter('entity_id', array(
				'header' => 'Is B2B Approved',
				'type' => 'text',
				'index' => 'entity_id'
			));
		}
	}
}