<?php 

class Urcover_B2b_Model_Options
{
	public function toOptionArray()
	{
		$items = array(
			array(
				'value' => 0,
				'label' => 'Disabled',
			)
		);
		$groups = Mage::getModel('customer/group')->getCollection();
		foreach ($groups as $type) {
			$items[] = array(
				'value' => $type->getCustomerGroupId(),
				'label' => $type->getCustomerGroupCode(),
			);
		}
		return $items;
	}
}