<?php 

class Urcover_B2b_Block_Register extends Mage_Core_Block_Template
{
	public function getCountriesList()
	{
		$countries = Mage::getModel('directory/country')->getCollection();
		$html = '';
		foreach ($countries as $country) {
			$html .= '<option value="'.$country->getCountryId().'">'.$country->getName().'</option>';
		}
		return $html;
	}
	
	public function getAjaxUrl()
	{
		return $this->helper('b2b')->getAjaxUrl();
	}
}