<?php 

class Urcover_B2b_Block_Adminhtml_Template_Grid_Renderer_B2b extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
	protected $group = null;
	
	public function __construct()
	{
		$this->group = intval(Mage::getStoreConfig('b2b/main_options/discount_group'));
		parent::__construct();
	}
	
	public function render(Varien_Object $row)
	{
		return $this->_getValue($row);
	}
	
	protected function _getValue(Varien_Object $row)
	{
		$cid = $row->getGroupId();
		if ($cid != $this->group) {
			return;
		}
		$connection = Mage::getSingleton('core/resource')->getConnection('core_read');
		$sql = 'SELECT * FROM `b2b_customers` WHERE `internal_id` = '.intval($row->getId());
		$row = $connection->fetchRow($sql);
		if (!isset($row['is_approved']) || intval($row['is_approved']) == 0) {
			return '<a href="'.Mage::helper('adminhtml')->getUrl('adminhtml/b2b/approve', array('c' => $row['customer_id'])).'">approve</a>';
		} else {
			return '<a href="'.Mage::helper('adminhtml')->getUrl('adminhtml/b2b/disable', array('c' => $row['customer_id'])).'">disable</a>';
		}
	}
}