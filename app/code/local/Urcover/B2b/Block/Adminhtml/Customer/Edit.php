<?php 

class Urcover_B2b_Block_Adminhtml_Customer_Edit extends Mage_Adminhtml_Block_Customer_Edit
{
	public function __construct()
	{
		parent::__construct();
		
		$discountGroup = Mage::getStoreConfig('b2b/main_options/discount_group');
		$groupId = Mage::registry('current_customer')->getGroupId();
		
		if ($groupId == intval($discountGroup)) {
			$connection = Mage::getSingleton('core/resource')->getConnection('core_read');
			$sql = 'SELECT * FROM `b2b_customers` WHERE `internal_id` = '.intval($this->getCustomerId());
			$row = $connection->fetchRow($sql);
			if (!isset($row['is_approved']) || intval($row['is_approved']) == 0) {
				$b2bLabel = 'Approve';
				$b2bClass = 'save';
				$b2bLocation = Mage::helper('adminhtml')->getUrl('adminhtml/b2b/approve', array('c' => $row['customer_id']));
			} else {
				$b2bLabel = 'Disapprove';
				$b2bClass = 'delete';
				$b2bLocation = Mage::helper('adminhtml')->getUrl('adminhtml/b2b/disable', array('c' => $row['customer_id']));
			}
			$this->_addButton('test', array(
				'label' => Mage::helper('b2b')->__($b2bLabel),
				'onclick' => 'setLocation(\'' . $b2bLocation . '\')',
				'class' => $b2bClass,
			), 10);
		}
	}
}